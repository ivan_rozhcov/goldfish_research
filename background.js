//https://developer.chrome.com/extensions/webNavigation#event-onCreatedNavigationTarget
chrome.webNavigation.onCreatedNavigationTarget.addListener(function(details) {
    // определяем нод id для sourceTabId и для TabId создаем нод
    chrome.storage.local.get('currentPlotId', function(obj){
        var currentPlotId = obj.currentPlotId;
        if(currentPlotId){
            var sourceTabId = String(details.sourceTabId);

            chrome.storage.local.get(sourceTabId, function(obj){
                var nodeShapeId = obj[sourceTabId];
                if(nodeShapeId){
                    // наглый гугл передает свои урлы - будем пытатсья их вычишать
                    var normalUrl = details.url;
                    if (details.url.search('google.com') != -1){
                        var normalUrlGuess = getParameterByName(details.url, 'url');
                        if (normalUrlGuess){
                           normalUrl = normalUrlGuess;
                        }
                    }
                    // TODO не менее наглый яндекс делает так же http://yandex.ru/clck/jsredir?from=yandex.ru%3Bsearch%2F%3Bweb%3B%3B&text=&etext=924.myZf7aEav5a_ZUNlat06pa5Yhoja8ahKIjZLjkUPKGSwaahBmulr7IAsRhbc5J5l.bcf9a2a918ceca5186d788304ccc9299f1498d79&uuid=&state=PEtFfuTeVD5kpHnK9lio9ThluiZ1G4ojQRnDqP1wRIkdnIy1TZuw1A&data=UlNrNmk5WktYejR0eWJFYk1LdmtxbGptOF9HUVhjRzBwX3V5WkpvUnRrX1c2NjgtUzdfZnVIQ2pxUXZfWHgyU3k1OE83Q3B1MElUMlA2aHpHbDVQbmdOVVhzN1U5VnhSNlZwbUxKT1dOTkE&b64e=2&sign=34aff7cb20c3ed9bd713e49b307e4949&keyno=0&cst=AiuY0DBWFJ5fN_r-AEszkyMOBXl1rrpYQUT_kU1sHvqr6Q-JA0o3iTlpl94Qsrp558Al2q1wC9t6xkNx8lbi_gZBJsEBFT4-sZMCekQBQhx0RZseO-E3cgIBGpHHnwXH5rTdb2HyT9NrviCtvyru82kjJLZvb5VDjtaXW2Wcf3QJsAsYqDr6HtZNxg-TQMQan2b_h5qV2or9IkX3P2GSW5d4GeAGnhdj3qo3QyYwmtc&ref=orjY4mGPRjk5boDnW0uvlrrd71vZw9kp5uQozpMtKCVtWsCrMpZ1dUQ1XtLPX6qK8z_4XjpXFj73g1c38Zd8izyI1Hx_X0_zWSba0ldozp1TcAkfoWemV4SSctxBJbpr0a31zFcvuQg&l10n=ru&cts=1451970004215&mc=4.67970767127841
                    addNodeShape('new tab', normalUrl, currentPlotId, function(result){
                        var newNodeShapeId = result .obj._id;
                        var obj = {};
                        obj[details.tabId] = newNodeShapeId;
                        // TODO сеттить что то типа new: true?
                        chrome.storage.local.set(obj);
                        //добавляем url
                        addLinkShape('opened', currentPlotId, nodeShapeId, newNodeShapeId)
                    });
                } else {
                    console.log('no watch');
                }
            });
        }

    });
}, {
}, ["blocking"]);