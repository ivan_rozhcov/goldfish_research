window.onload = function () {
    chrome.storage.local.get('isResearchEnabled', function (obj) {
        var result = obj.isResearchEnabled;
        if (result) {
            document.getElementById("title").textContent = "Enabled: " + result;
            document.getElementById("plot_name").disabled = true;
            document.getElementById("followButton").disabled = true;
        } else {
            document.getElementById("title").textContent = "Disabled";
            document.getElementById("addButton").disabled = true;
            document.getElementById("unfollowButton").disabled = true;
        }
    });
    //TODO check current tabId & show information about nodeshape

    var followButton = document.getElementById("followButton");
    followButton.addEventListener("click", function () {
        chrome.storage.local.clear();
        document.getElementById("title").textContent = "Enabled";
        var plotName = document.getElementById("plot_name").value;

        addPlot(plotName, function (result) {
            document.getElementById('title').textContent = 'OK';
            chrome.storage.local.set({currentPlotId: result.obj._id});
            // исследование началось
            document.getElementById("plot_name").disabled = true;
            document.getElementById("followButton").disabled = true;

            document.getElementById("addButton").disabled = false;
            document.getElementById("unfollowButton").disabled = false;
        });
        chrome.storage.local.set({isResearchEnabled: plotName});
//        window.close();
    }, false);

    var addButton = document.getElementById("addButton");
    addButton.addEventListener("click", function () {
        chrome.storage.local.get('currentPlotId', function (obj) {
            var currentPlotId = obj.currentPlotId;
            if (currentPlotId) {
                var query = { active: true, currentWindow: true};

                function callback(tabs) {
                    var currentTab = tabs[0];
                    var currentTabId = String(currentTab['id']);
                    console.log(currentTab);

                    chrome.storage.local.get(currentTabId, function (obj) {
                        var nodeShapeId = obj[currentTabId];
                        if (nodeShapeId) {
                            console.log('already in goldfish');
                        } else {
                            addNodeShape(currentTab.title, currentTab.url, currentPlotId, function (result) {
                                var obj = {};
                                obj[currentTabId] = result.obj._id;
                                chrome.storage.local.set(obj);
                                document.getElementById('title').textContent = 'OK';
                            });
                        }
                    });
                }

                chrome.tabs.query(query, callback);
            }

        });
    }, false);

    var unfollowButton = document.getElementById("unfollowButton");
    unfollowButton.addEventListener("click", function () {
        document.getElementById("title").textContent = "Disabled";
        chrome.storage.local.clear();
        window.close();
    }, false);
};

