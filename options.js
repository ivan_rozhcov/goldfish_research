function loadOptions() {

    var select = document.getElementById("plot-type");
    for (var i = 0; i < select.children.length; i++) {
        var child = select.children[i];
        if (child.value == localStorage["plotType"]) {
            child.selected = "true";
            break;
        }
    }
    document.getElementById("user-id").value = localStorage["userId"];
    document.getElementById("default-title").value = localStorage["defaultTitle"];
    document.getElementById("base-url").value = localStorage["BASE_URL"] || 'localhost:3000';
    document.getElementById("authorization-token").value = localStorage["AUTHORIZATION_TOKEN"] || '97f0ad9e24ca5e0408a269748d7fe0a0';
}

function saveOptions() {
    var select = document.getElementById("plot-type");
    var plotType = select.children[select.selectedIndex].value;
    var userId = document.getElementById("user-id").value;
    var defaultTitle = document.getElementById("default-title").value;
    var baseUrl = document.getElementById("base-url").value;
    var authorizationToken = document.getElementById("authorization-token").value;
    localStorage["userId"] = userId;
    localStorage["plotType"] = plotType;
    localStorage["defaultTitle"] = defaultTitle;
    localStorage["BASE_URL"] = baseUrl || 'localhost:3000';
    localStorage["AUTHORIZATION_TOKEN"] = authorizationToken || '97f0ad9e24ca5e0408a269748d7fe0a0';
}


window.onload = function () {
    loadOptions();
    var addButton = document.getElementById("saveButton");
    addButton.addEventListener("click", function () {
        saveOptions()
    });

};