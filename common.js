//общие библиотеки для работы с goldfish


function getParameterByName(url, name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(url);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function addPlot(title, successHandler) {

    document.getElementById('title').textContent = 'Adding...';
    var myData = {
        "title": title || localStorage["defaultTitle"],
        "for_user": localStorage["userId"],
        "type": localStorage["plotType"]
    };

    var xhr = new XMLHttpRequest();
    xhr.open("POST", 'http://' + localStorage["BASE_URL"] + '/api/plots/', true);
    xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
    xhr.setRequestHeader('X-Auth-Token', localStorage['AUTHORIZATION_TOKEN']);

    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 201) {
            var data = JSON.parse(xhr.responseText);
            successHandler && successHandler(data);
        }
        else {
            console.log("Error: state: " + xhr.readyState + " status: " + xhr.status);
            document.getElementById('title').textContent = "Error: state: " + xhr.readyState + " status: " + xhr.status;
        }
    };

    // Отсылаем объект в формате JSON и с Content-Type application/json
    // Сервер должен уметь такой Content-Type принимать и раскодировать
    xhr.send(JSON.stringify(myData));
}

function addNodeShape(title, url, plotId, successHandler) {
    var myData = {
        "content": null,
        "plot_id": plotId,
        "title": title || "new node",
        "without_node_shape": false,
        "for_user": localStorage["userId"],
        "content_type": "url",
        "widget_type": "view_only",
        "node_type": "url",
        "x": 150,
        "y": 50,
        "zoom": 1,
        "view_type": "nsl_url",
        "node_external_uri": url
    };

    var xhr = new XMLHttpRequest();
    xhr.open("POST", 'http://' + localStorage["BASE_URL"] + '/api/node_shapes/', true);
    xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
    xhr.setRequestHeader('X-Auth-Token', localStorage['AUTHORIZATION_TOKEN']);

    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 201) {
            var data = JSON.parse(xhr.responseText);
            successHandler && successHandler(data);
        }
        else {
            //    todo значок меняется показывает ошибку
            console.log("Error: state: " + xhr.readyState + " status: " + xhr.status);
//            НЕЛЬЗЯ тк эта функция вызывается и из popup js
//            document.getElementById('title').textContent = "Error: state: " + xhr.readyState + " status: " +  xhr.status;
        }
    };

    // Отсылаем объект в формате JSON и с Content-Type application/json
    // Сервер должен уметь такой Content-Type принимать и раскодировать
    xhr.send(JSON.stringify(myData));
}

function addLinkShape(title, plotId, nodeShapeFromId, nodeShapeToId, successHandler) {
    var myData = {
        "plot_id": plotId,
        "title": title || "new link",
        "for_user": localStorage["userId"],
        "node_shape_from_id": nodeShapeFromId,
        "node_shape_to_id": nodeShapeToId,
        "type": "user"
    };

    var xhr = new XMLHttpRequest();
    xhr.open("POST", 'http://' + localStorage["BASE_URL"] + '/api/link_shapes/', true);
    xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
    xhr.setRequestHeader('X-Auth-Token', localStorage['AUTHORIZATION_TOKEN']);

    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 201) {
            //    todo значок меняется на ok
            var data = JSON.parse(xhr.responseText);
            successHandler && successHandler(data);
//            return xhr.responseText
        }
        else {
            //    todo значок меняется показывает ошибку
            console.log("Error: state: " + xhr.readyState + " status: " + xhr.status);
        }
    };

    // Отсылаем объект в формате JSON и с Content-Type application/json
    // Сервер должен уметь такой Content-Type принимать и раскодировать
    xhr.send(JSON.stringify(myData));
}